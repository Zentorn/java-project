package game;

//https://stackoverflow.com/questions/23510648/javafx-array-of-controls

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;

public class JavaFX extends Application {

	@Override
	public void start(Stage primaryStage) {

		Group root = new Group();
		root.getChildren().add(getGrid());
		Scene scene = new Scene(root, 800, 600);
		primaryStage.setTitle("Game of the year");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}

	private Pane getGrid() {

		GridPane gridPane = new GridPane();
		gridPane.setAlignment(Pos.CENTER);
		gridPane.setPrefSize(400, 400);
		gridPane.setMaxSize(Region.USE_COMPUTED_SIZE, Region.USE_COMPUTED_SIZE);
		for (int k = 0; k < 7; k++) {
			for (int j = 0; j < 7; j++) {
				gridPane.add(new ToggleButton("button" + k + j), k, j);
				// column=2 row=1
				gridPane.getColumnConstraints().add(new ColumnConstraints(80));
				gridPane.getRowConstraints().add(new RowConstraints(45));
			}
		}
return gridPane;
	}
}
