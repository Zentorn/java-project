package game;

public class Methods {
	
	// Sum method that calculates the sum of the column
	public static int sum(int sum, int x[][], int o) {
		for (int i = 0; i < x.length; i++) {
			int p = x[i][o];
			sum = sum + p;
		}
		return sum;
	}

	// Sum method that calculates the sum of the row
	public static int rsum(int sum, int x[][], int o) {
		for (int i = 0; i < x.length; i++) {
			int p = x[o][i];
			sum = sum + p;
		}
		return sum;
	}
}
