package game;

import java.util.Arrays;

public class NumGame {

	static int tableSize = 7;

	public static void main(String[] args) {

		// Generation on acol array
		int[] acol;
		acol = new int[tableSize];
		for (int i = 0; i < acol.length; i++) {
			int nr = (int) (Math.random() * 21);
			if (nr < 1) {
				nr = +1;
			}
			acol[i] = nr;
		}
		
		System.out.println(Arrays.toString(acol));
		System.out.println();

		// Generation of arr arrayy, author J. Pöial
		int[][] arr;
		arr = new int[tableSize][tableSize];
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++)
				System.out.print(String.valueOf(arr[i][j]) + " ");
			System.out.println();
		}
		System.out.println();

		// Generates random numbers to arr array and places them to a random row in the current column
		for (int col = 0; col < tableSize; col++) {
			int n = (acol[col] - (Methods.sum(0, arr, col)));
			while (n > 0) {
				int rand = (int) (Math.random() * 21);
				if (rand > n) {
					rand = (int) (Math.random() * 21);
				}
				int randrow = (int) (Math.random() * tableSize);
				if ((arr[randrow][col]) < 1) {
					arr[randrow][col] = (int) (Math.random() * n + 1);
				}
				n = (acol[col] - (Methods.sum(0, arr, col)));
			}

			// Prints out arr array as a table, source stackowerflow
			System.out.println(Arrays.deepToString(arr).replace("], ", "]\n"));
			System.out.println();
		}

		// Generation of arow array
		int[] arow;
		arow = new int[tableSize];
		for (int i = 0; i < arow.length; i++) {
			arow[i] = Methods.rsum(0, arr, i);
			System.out.println(arow[i]);
		}
		System.out.println();
		
		// Replaces the remaining zeros in arr array with random numbers
		for (int i = 0; i < tableSize; i++) {
			for (int j = 0; j < tableSize; j++) {
				if (arr[i][j] == 0) {
					int nr = (int) (Math.random() * acol[i]);
					if (nr == 0) {
						nr = +1 + ((int) (Math.random() * 20));
					}
					arr[i][j] = nr;
				}
			}
		}
		System.out.println(Arrays.toString(acol));
		System.out.println();
		System.out.println(Arrays.deepToString(arr).replace("], ", "]\n"));
		System.out.println();

	}
}
